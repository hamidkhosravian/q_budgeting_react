import axios from 'axios';

var axiosInstance = axios.create({
    baseURL: 'http://localhost:5000/',
});

export default {
  user: {
    sign_in: (user) => axiosInstance.post('/api/v1/sign_in', user).then(res => res.data),
    forgot_password: (user) => axiosInstance.post('/api/v1/forgot_password', user).then(res => res),
    reset_password: (user) => axiosInstance.post('/api/v1/reset_password', user).then(res => res),
  }
}
