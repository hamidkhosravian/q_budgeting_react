import React from "react";
import PropTypes from "prop-types";
import ReactTable from "react-table";

// reactstrap components
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  CardTitle,
  Row,
  Col
} from "reactstrap";
import ReactBSAlert from "react-bootstrap-sweetalert";

import defaultAvatar from "assets/img/placeholder.jpg";

const dataTable = [
  ["Tiger Nixon", "System Architect", "Edinburgh", defaultAvatar],
  ["Garrett Winters", "Accountant", "Tokyo", defaultAvatar],
  ["Ashton Cox", "Junior Technical Author", "San Francisco", defaultAvatar],
  ["Cedric Kelly", "Senior Javascript Developer", "Edinburgh", defaultAvatar],
  ["Airi Satou", "Accountant", "Tokyo", defaultAvatar],
  ["Brielle Williamson", "Integration Specialist", "New York", defaultAvatar],
  ["Herrod Chandler", "Sales Assistant", "San Francisco", defaultAvatar],
  ["Rhona Davidson", "Integration Specialist", "Tokyo", defaultAvatar],
  ["Colleen Hurst", "Javascript Developer", "San Francisco", defaultAvatar],
  ["Sonya Frost", "Software Engineer", "Edinburgh", defaultAvatar],
  ["Jena Gaines", "Office Manager", "London", defaultAvatar],
  ["Quinn Flynn", "Support Lead", "Edinburgh", defaultAvatar],
  ["Charde Marshall", "Regional Director", "San Francisco", defaultAvatar],
  ["Haley Kennedy", "Senior Marketing Designer", "London", defaultAvatar],
  ["Tatyana Fitzpatrick", "Regional Director", "London", defaultAvatar],
  ["Michael Silva", "Marketing Designer", "London", defaultAvatar],
  ["Paul Byrd", "Chief Financial Officer (CFO)", "New York", defaultAvatar],
  ["Gloria Little", "Systems Administrator", "New York", defaultAvatar],
  ["Bradley Greer", "Software Engineer", "London", defaultAvatar],
  ["Dai Rios", "Personnel Lead", "Edinburgh", defaultAvatar],
  ["Jenette Caldwell", "Development Lead", "New York", defaultAvatar],
  ["Yuri Berry", "Chief Marketing Officer (CMO)", "New York", defaultAvatar],
  ["Caesar Vance", "Pre-Sales Support", "New York", defaultAvatar],
  ["Doris Wilder", "Sales Assistant", "Sidney", defaultAvatar],
  ["Angelica Ramos", "Chief Executive Officer (CEO)", "London", defaultAvatar],
  ["Gavin Joyce", "Developer", "Edinburgh", defaultAvatar],
  ["Jennifer Chang", "Regional Director", "Singapore", defaultAvatar],
  ["Brenden Wagner", "Software Engineer", "San Francisco", defaultAvatar],
  ["Fiona Green", "Chief Operating Officer (COO)", "San Francisco", defaultAvatar],
  ["Shou Itou", "Regional Marketing", "Tokyo", defaultAvatar],
  ["Michelle House", "Integration Specialist", "Sidney", defaultAvatar],
  ["Suki Burks", "Developer", "London", defaultAvatar],
  ["Prescott Bartlett", "Technical Author", "London", defaultAvatar],
  ["Gavin Cortez", "Team Leader", "San Francisco", defaultAvatar],
  ["Martena Mccray", "Post-Sales support", "Edinburgh", defaultAvatar],
  ["Unity Butler", "Marketing Designer", "San Francisco", defaultAvatar],
  ["Howard Hatfield", "Office Manager", "San Francisco", defaultAvatar],
  ["Hope Fuentes", "Secretary", "San Francisco", defaultAvatar],
  ["Vivian Harrell", "Financial Controller", "San Francisco", defaultAvatar],
  ["Timothy Mooney", "Office Manager", "London", defaultAvatar],
  ["Jackson Bradshaw", "Director", "New York", defaultAvatar],
  ["Olivia Liang", "Support Engineer", "Singapore", defaultAvatar]
];

class UserIndex extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      alert: null,
      data: dataTable.map((prop, key) => {
        return {
          id: key,
          image: prop[3],
          name: prop[0],
          position: prop[1],
          email: prop[2],
          actions: (
            <div className="actions-right text-center">
              <Button
                color="warning"
                size="sm"
                className="btn-icon btn-link edit"
                onClick={e => this.showEditProfilePage(key)}
              >
                <i className="fa fa-edit" style={{fontSize: 16}} />
              </Button>{" "}
              <Button
                color="danger"
                size="sm"
                className="btn-icon btn-link remove"
                onClick={this.warningWithConfirmAndCancelMessage}
              >
                <i className="fa fa-times" style={{fontSize: 16}} />
              </Button>{" "}
            </div>
          )
        };
      })
    };
  }

  showEditProfilePage = (id) => {
    this.props.history.push(`/admin/users/profile/${id}`)
  }

  warningWithConfirmAndCancelMessage = () => {
    this.setState({
      alert: (
        <ReactBSAlert
          warning
          style={{ display: "block", marginTop: "-100px" }}
          title="Are you sure?"
          onConfirm={() => this.successDelete()}
          onCancel={() => this.cancelDetele()}
          confirmBtnBsStyle="info"
          cancelBtnBsStyle="danger"
          confirmBtnText="Yes, delete it!"
          cancelBtnText="Cancel"
          showCancel
        >
          You will not be able to recover this User!
        </ReactBSAlert>
      )
    });
  };

  successDelete = () => {
    this.setState({
      alert: (
        <ReactBSAlert
          success
          style={{ display: "block", marginTop: "-100px" }}
          title="Deleted!"
          onConfirm={() => this.hideAlert()}
          onCancel={() => this.hideAlert()}
          confirmBtnBsStyle="info"
        >
          Your User has been deleted.
        </ReactBSAlert>
      )
    });
  };

  cancelDetele = () => {
    this.setState({
      alert: (
        <ReactBSAlert
          danger
          style={{ display: "block", marginTop: "-100px" }}
          title="Cancelled"
          onConfirm={() => this.hideAlert()}
          onCancel={() => this.hideAlert()}
          confirmBtnBsStyle="info"
        >
          Your User is safe.
        </ReactBSAlert>
      )
    });
  };

  hideAlert = () => {
    this.setState({
      alert: null
    });
  };

  imageAvatar = (avatar) => {
    return (
      <div className="text-center">
        <div className="fileinput">
          <div className="thumbnail img-circle">
            <img src={avatar} alt="..." />
          </div>
        </div>
      </div>
   )
  }

  textView = (txt) => {
    return (
      <div className="text-center">
        <span>{txt}</span>
      </div>
    )
  }

  render() {
    return (
      <>
        <div className="content">
        {this.state.alert}
          <Row>
            <Col md="12">
              <Card>
                <CardHeader>
                  <Row>
                    <Col sm="10">
                      <CardTitle tag="h4">React-Tables</CardTitle>
                    </Col>
                    <Col sm="2">
                      <div>
                        <Button className="btn-round" color="primary">
                          Create New User
                        </Button>
                      </div>
                    </Col>
                  </Row>
                </CardHeader>
                <CardBody>
                  <ReactTable
                    data={this.state.data}
                    filterable
                    columns={[
                      {
                        Header: "Image",
                        accessor: "image",
                        Cell: (row) => {
                          const avatar = row.original.image? row.original.image : defaultAvatar
                          return  this.imageAvatar(avatar)
                        },
                        sortable: false,
                        filterable: false,
                        headerClassName: "text-center"
                      },
                      {
                        Header: "Name",
                        accessor: "name",
                        Cell: (row) => {
                          return  this.textView(row.original.name)
                        },
                        headerClassName: "text-center"
                      },
                      {
                        Header: "Position",
                        accessor: "position",
                        Cell: (row) => {
                          return  this.textView(row.original.position)
                        },
                        headerClassName: "text-center"
                      },
                      {
                        Header: "Email",
                        accessor: "email",
                        Cell: (row) => {
                          return  this.textView(row.original.email)
                        },
                        headerClassName: "text-center"
                      },
                      {
                        Header: "Actions",
                        accessor: "actions",
                        sortable: false,
                        filterable: false,
                        headerClassName: "text-center"
                      }
                    ]}
                    defaultPageSize={10}
                    showPaginationBottom
                    className="-striped -highlight primary-pagination"
                  />
                </CardBody>
              </Card>
            </Col>
          </Row>
        </div>
      </>
    );
  }
}



UserIndex.propTypes = {
  history: PropTypes.shape({
    push: PropTypes.func.isRequired
  }).isRequired,
};

export default UserIndex;
