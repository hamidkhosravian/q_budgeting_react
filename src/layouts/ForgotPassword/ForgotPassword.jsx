import React from "react";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
// core components
import {
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  Row,
  Col,
  InputGroup,
  InputGroupText,
  InputGroupAddon,
  Input,
  Alert
} from "reactstrap";

import Validator from "validator";

import PropTypes from "prop-types";
import { connect } from "react-redux";

import Button from "../../components/CustomButton/CustomButton.jsx";
import loginPageStyle from "assets/jss/material-kit-react/views/loginPage.jsx";
import image from "assets/img/bg7.jpg";

import { forgot_password } from "../../actions/authentication";

class ForgotPassword extends React.Component {
  constructor(props) {
    super(props);
    // we use this to make the card to appear after the page has been rendered
    this.state = {
      cardAnimaton: "cardHidden",
      data: {
        email: "",
        password: ""
      },
      errors: {},
      visible: true
    };

    this.onDismiss = this.onDismiss.bind(this);
  }

  componentDidMount() {
    // we add a hidden class to the card and after 700 ms we delete it and the transition appears
    setTimeout(
      function() {
        this.setState({ cardAnimaton: "" });
      }.bind(this),
      700
    );
  }

  onChange = e =>
    this.setState({
      data: { ...this.state.data, [e.target.name]: e.target.value }
  });

  submit = () => {
    const errors = this.validate(this.state.data);
    this.setState({ errors });

    if (Object.keys(errors).length === 0) {
      this.props
        .forgot_password(this.state.data)
        .then(() => this.props.history.push("/"))
        .catch(err => {
          const error = err.response ? err.response.data : err.message;
          this.setState({ errors: { response: error }, visible: true });
        });
    }
  };

  validate = data => {
    const errors = {};
    if (!Validator.isEmail(data.email)) errors.email = "Email is wrong";
    return errors;
  };

  onDismiss() {
    this.setState({ visible: false });
  }

  showErrorResponse = (message) => {
    return (
      <div>
        <Alert
          place={'bc'}
          color="danger"
          isOpen={this.state.visible}
          toggle={this.onDismiss}
        >
          <span>{message}</span>
        </Alert>
      </div>
    )
  }

  render() {
    const { errors } = this.state;
    const { classes } = this.props;

    return (
      <div className="content">
        <div
          className={classes.pageHeader}
          style={{
            backgroundImage: "url(" + image + ")",
            backgroundSize: "cover",
            backgroundPosition: "top center"
          }}
        >
          <div className={classes.container}>
            <Row>
              <Col sm="12" md={{ size: 4, offset: 4 }} >
                <Card className={classes[this.state.cardAnimaton]}>
                  <div className={classes.form}>
                    <CardHeader color="primary" className={classes.cardHeader}>
                      <h4>Forgot Password</h4>
                    </CardHeader>
                    <CardBody>
                      <div>
                        <InputGroup style={{marginBottom: 16}}>
                          <Input onChange={this.onChange} placeholder="Email" type="email" name="email"/>
                          <InputGroupAddon addonType="append">
                            <InputGroupText>
                              <i className="nc-icon nc-email-85" />
                            </InputGroupText>
                          </InputGroupAddon>
                        </InputGroup>

                        {
                          errors.email &&
                          <p className="text-danger">{errors.email}</p>
                        }
                      </div>

                      <Row>
                        <div className="update ml-auto mr-auto">
                          <Button onClick={this.submit} color="primary" round>Forgot Password</Button>
                        </div>
                      </Row>

                      <Row>
                        <div className="update ml-auto mr-auto">
                          <Button color="primary" link round onClick={() => this.props.history.push("/sign_in")}>Sign In</Button>
                        </div>
                      </Row>

                      <CardFooter className={classes.cardFooter}>
                      </CardFooter>
                    </CardBody>
                  </div>
                </Card>

                {
                  errors.response &&
                  this.showErrorResponse(errors.response)
                }
              </Col>
            </Row>

          </div>
        </div>
      </div>
    );
  }
}

ForgotPassword.propTypes = {
  history: PropTypes.shape({
    push: PropTypes.func.isRequired
  }).isRequired,
  forgot_password: PropTypes.func.isRequired
};

export default withStyles(loginPageStyle)(connect(null, { forgot_password })(ForgotPassword));
