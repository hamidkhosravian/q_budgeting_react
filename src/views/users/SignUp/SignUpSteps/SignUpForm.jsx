import React from "react";
import classnames from "classnames";
// reactstrap components
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  CardTitle,
  Label,
  FormGroup,
  Form,
  Input,
  Row,
  Col
} from "reactstrap";

import Switch from "react-bootstrap-switch";

class SignUpForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      registerEmail: "",
      registerPassword: "",
      registerConfirmPassword: "",
      registerEmailState: "",
      registerPasswordState: "",
      registerConfirmPasswordState: ""
    };
  }

  verifyEmail = value => {
    var emailRex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (emailRex.test(value)) {
      return true;
    }
    return false;
  };

  verifyLength = (value, length) => {
    if (value.length >= length) {
      return true;
    }
    return false;
  };

  compare = (string1, string2) => {
    if (string1 === string2) {
      return true;
    }
    return false;
  };

  change = (event, stateName, type, stateNameEqualTo, maxValue) => {
    switch (type) {
      case "email":
        if (this.verifyEmail(event.target.value)) {
          this.setState({ [stateName + "State"]: "has-success" });
        } else {
          this.setState({ [stateName + "State"]: "has-danger" });
        }
        break;
      case "password":
        if (this.verifyLength(event.target.value, 1)) {
          this.setState({ [stateName + "State"]: "has-success" });
        } else {
          this.setState({ [stateName + "State"]: "has-danger" });
        }
        break;
      case "equalTo":
        if (this.compare(event.target.value, this.state[stateNameEqualTo])) {
          this.setState({ [stateName + "State"]: "has-success" });
          this.setState({ [stateNameEqualTo + "State"]: "has-success" });
        } else {
          this.setState({ [stateName + "State"]: "has-danger" });
          this.setState({ [stateNameEqualTo + "State"]: "has-danger" });
        }
        break;
      default:
        break;
    }
    this.setState({ [stateName]: event.target.value });
  };

  isValidated = () => {
    if (
      this.state.registerEmailState === "has-success" &&
      this.state.registerPasswordState === "has-success" &&
      this.state.registerConfirmPasswordState === "has-success"
    ) {
      return true;
    } else {
      if (this.state.registerEmailState === "") {
        this.setState({ registerEmailState: "has-danger" });
      }
      if (
        this.state.registerPasswordState === "" ||
        this.state.registerConfirmPasswordState === ""
      ) {
        this.setState({ registerPasswordState: "has-danger" });
        this.setState({ registerConfirmPasswordState: "has-danger" });
      }
    }
  };

  render() {
    const {
      registerEmailState,
      registerPasswordState,
      registerConfirmPasswordState,
    } = this.state;

    return (
      <>
        <div className="content">
        <Row className="justify-content-center">
          <Col sm="6">
            <Form>
              <Col sm="12">
                <FormGroup className={`has-label ${registerEmailState}`}>
                  <label>Email Address *</label>
                  <Input
                    name="email"
                    type="email"
                    onChange={e => this.change(e, "registerEmail", "email")}
                  />
                  {this.state.registerEmailState === "has-danger" ? (
                    <label className="error">
                      Please enter a valid email address.
                    </label>
                  ) : null}
                </FormGroup>
              </Col>
              <Col sm="12">
                <FormGroup className={`has-label ${registerPasswordState}`}>
                  <label>Password *</label>
                  <Input
                    id="registerPassword"
                    name="password"
                    type="password"
                    autoComplete="off"
                    onChange={e =>
                      this.change(e, "registerPassword", "password")
                    }
                  />
                  {this.state.registerPasswordState === "has-danger" ? (
                    <label className="error">This field is required.</label>
                  ) : null}
                </FormGroup>
              </Col>
              <Col sm="12">
                <FormGroup
                  className={`has-label ${registerConfirmPasswordState}`}
                >
                  <label>Confirm Password *</label>
                  <Input
                    equalto="#registerPassword"
                    id="registerPasswordConfirmation"
                    name="password_confirmation"
                    type="password"
                    autoComplete="off"
                    onChange={e =>
                      this.change(
                      e,
                      "registerConfirmPassword",
                      "equalTo",
                      "registerPassword"
                      )
                    }
                  />
                  {this.state.registerConfirmPasswordState ===
                  "has-danger" ? (
                    <label className="error">This field is required.</label>
                  ) : null}
                </FormGroup>
              </Col>
              <Col sm="12">
                <FormGroup>
                  <Row>
                    <Col md="5" sm="12">
                      <Label >
                        User can Sign In at the System?
                      </Label>
                    </Col>
                    <Col md="1" sm="12">
                      <Switch
                        defaultValue={true}
                        offColor="success"
                        offText={<i className="nc-icon nc-simple-remove" />}
                        onColor="success"
                        onText={<i className="nc-icon nc-check-2" />}
                      />
                    </Col>
                  </Row>
                </FormGroup>
              </Col>
            </Form>
          </Col>
        </Row>
      </div>
    </>
    );
  }
}

export default SignUpForm;


// <div>
//   <Button color="primary" onClick={this.registerClick}>
//     Register
//   </Button>
// </div>
