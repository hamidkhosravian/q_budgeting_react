import api from '../api';
import { USER_LOGGED_IN, MESSAGE } from '../types';
import setAuthorizationHeader from '../utils/setAuthorizationHeader';

export const userLoggedIn = user => ({
  type: USER_LOGGED_IN,
  user
});

export const showMessage = (message) => ({
  type: MESSAGE,
  message
});

export const sign_in = (data) => (dispatch) =>
  api.user.sign_in(data)
  .then(user => {
    localStorage.token = user.token.token;
    setAuthorizationHeader(user.token.token);
    dispatch(userLoggedIn(user))
});

export const forgot_password = (data) => (dispatch) =>
  api.user.forgot_password(data).then(message => {
    dispatch(showMessage(message.data))
  }
);

export const reset_password = (data) => (dispatch) =>
  api.user.reset_password(data).then(message => {
    dispatch(showMessage(message.data))
  }
);
