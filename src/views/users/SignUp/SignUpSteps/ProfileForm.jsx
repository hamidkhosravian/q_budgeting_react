import React from "react";
import classnames from "classnames";
// reactstrap components
import {
  Input,
  Form,
  Button,
  FormGroup,
  InputGroupAddon,
  InputGroupText,
  InputGroup,
  Row,
  Col
} from "reactstrap";

// core components
import PictureUpload from "components/CustomUpload/PictureUpload.jsx";

class ProfileForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      firstname: "",
      lastname: "",
      firstnameState: "",
      lastnameState: "",
      personalcodeState: ""
    };
  }

  // function that verifies if a string has a given length or not
  verifyLength = (value, length) => {
    if (value.length >= length) {
      return true;
    }
    return false;
  };

  change = (event, stateName, type, stateNameEqualTo, maxValue) => {
    switch (type) {
      case "length":
        if (this.verifyLength(event.target.value, stateNameEqualTo)) {
          this.setState({ [stateName + "State"]: "has-success" });
        } else {
          this.setState({ [stateName + "State"]: "has-danger" });
        }
        break;
      default:
        break;
    }
    this.setState({ [stateName]: event.target.value });
  };

  isValidated = () => {
    if (
      this.state.firstnameState === "has-success" &&
      this.state.lastnameState === "has-success" &&
      this.state.personalcodeState === "has-success"
    ) {
      return true;
    } else {
      if (this.state.firstnameState !== "has-success") {
        this.setState({ firstnameState: "has-danger" });
      }
      if (this.state.lastnameState !== "has-success") {
        this.setState({ lastnameState: "has-danger" });
      }
      if (this.state.personalcodeState !== "has-success") {
        this.setState({ personalcodeState: "has-danger" });
      }
      return false;
    }
  };

  render() {
    return (
      <>
        <h5 className="info-text">
          Let's start with the basic information (with validation)
        </h5>
        <Row className="justify-content-center">
          <Col sm="4">
            <PictureUpload />
          </Col>
          <Col sm="6">
            <InputGroup
              className={classnames(this.state.firstnameState, {
                "input-group-focus": this.state.firstnameFocus
              })}
            >
              <InputGroupAddon addonType="prepend">
                <InputGroupText>
                  <i className="nc-icon nc-single-02" />
                </InputGroupText>
              </InputGroupAddon>
              <Input
                name="firstname"
                placeholder="First Name (required)"
                type="text"
                onChange={e => this.change(e, "firstname", "length", 3)}
                onFocus={e => this.setState({ firstnameFocus: true })}
                onBlur={e => this.setState({ firstnameFocus: false })}
              />
              {this.state.firstnameState === "has-danger" ? (
                <label className="error">This field is required.</label>
              ) : null}
            </InputGroup>
            <InputGroup
              className={classnames(this.state.lastnameState, {
                "input-group-focus": this.state.lastnameFocus
              })}
            >
              <InputGroupAddon addonType="prepend">
                <InputGroupText>
                  <i className="nc-icon nc-circle-10" />
                </InputGroupText>
              </InputGroupAddon>
              <Input
                name="lastname"
                placeholder="Last Name (required)"
                type="text"
                onChange={e => this.change(e, "lastname", "length", 3)}
                onFocus={e => this.setState({ lastnameFocus: true })}
                onBlur={e => this.setState({ lastnameFocus: false })}
              />
              {this.state.lastnameState === "has-danger" ? (
                <label className="error">This field is required.</label>
              ) : null}
            </InputGroup>
            <InputGroup
              className={classnames(this.state.personalcodeState, {
                "input-group-focus": this.state.personalcodeFocus
              })}
            >
              <InputGroupAddon addonType="prepend">
                <InputGroupText>
                  <i className="nc-icon nc-touch-id" />
                </InputGroupText>
              </InputGroupAddon>
              <Input
                name="Personal Code"
                placeholder="Personal Code (required)"
                type="text"
                onChange={e => this.change(e, "personalcode", "length", 3)}
                onFocus={e => this.setState({ personalcodeFocus: true })}
                onBlur={e => this.setState({ personalcodeFocus: false })}
              />
              {this.state.personalcodeState === "has-danger" ? (
                <label className="error">This field is required.</label>
              ) : null}
            </InputGroup>
          </Col>
          <Col className="mt-3" lg="10">
            <Form>
              <Row>
                <Col className="pr-1" md="4">
                  <FormGroup>
                    <label>City</label>
                    <Input
                      placeholder="City"
                      type="text"
                    />
                  </FormGroup>
                </Col>
                <Col className="px-1" md="4">
                  <FormGroup>
                    <label>State</label>
                    <Input
                      placeholder="State"
                      type="text"
                    />
                  </FormGroup>
                </Col>
                <Col className="pl-1" md="4">
                  <FormGroup>
                    <label>Postal Code</label>
                    <Input placeholder="Postal Code" type="number" />
                  </FormGroup>
                </Col>
              </Row>
              <Row>
                <Col md="8">
                  <FormGroup>
                    <label>Address</label>
                    <Input
                      placeholder="Home Address"
                      type="text"
                    />
                  </FormGroup>
                </Col>
                <Col className="pl-1" md="4">
                  <FormGroup>
                    <label>Phone Number</label>
                    <Input placeholder="Phone Number" type="number" />
                  </FormGroup>
                </Col>
              </Row>
              <Row>
                <Col md="12">
                  <FormGroup>
                    <label>Description</label>
                    <Input
                      className="textarea"
                      type="textarea"
                      cols="80"
                      rows="4"
                      placeholder="User Description"
                    />
                  </FormGroup>
                </Col>
              </Row>
            </Form>
          </Col>
        </Row>
      </>
    );
  }
}

export default ProfileForm;
