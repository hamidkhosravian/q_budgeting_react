import React from "react";
import thunk from 'redux-thunk';
import ReactDOM from "react-dom";
import { Provider } from 'react-redux';
import { createBrowserHistory } from "history";
import { createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import { Router, Route, Switch, Redirect } from "react-router-dom";

import "bootstrap/dist/css/bootstrap.css";
import "assets/scss/paper-dashboard.scss";
import "assets/demo/demo.css";

import rootReducer from './rootReducer';
import GuestRoute from "./routes/GuestRoute";
import UserRoute from "./routes/UserRoute";
import { userLoggedIn } from "./actions/authentication";
import setAuthorizationHeader from "./utils/setAuthorizationHeader";

import baseRoutes from "./baseRoutes";

import "bootstrap/dist/css/bootstrap.css";
import "assets/scss/paper-dashboard.scss";
import "assets/demo/demo.css";


const store = createStore(
  rootReducer,
  composeWithDevTools(applyMiddleware(thunk))
);

if(localStorage.token){
  const user = {
   token: localStorage.token
  };

  setAuthorizationHeader(localStorage.token);
  store.dispatch(userLoggedIn(user));
}

const hist = createBrowserHistory();

ReactDOM.render(
  <Router history={hist}>
    <Provider store={store}>
      <Switch>
        {
          baseRoutes.map((prop, key) => {
            if(prop.path === "/") {
              return <Redirect key={key} from="/" to="/admin/dashboard" />
            }else if(prop.route === "guest"){
              return <GuestRoute key={key} path={prop.path} component={prop.component} />
            }else if (prop.route === "user") {
              return <UserRoute key={key} path={prop.path} component={prop.component} />
            }else if (prop.route === "route") {
              return <Route key={key} path={prop.path} component={prop.component} />
            }
          })
        }
      </Switch>
    </Provider>
  </Router>,
  document.getElementById("root")
);
