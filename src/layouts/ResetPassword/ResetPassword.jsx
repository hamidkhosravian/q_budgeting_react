import React from "react";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
// core components
import {
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  Row,
  Col,
  InputGroup,
  InputGroupText,
  InputGroupAddon,
  Input,
  Alert
} from "reactstrap";

import * as qs from 'query-string';
import PropTypes from "prop-types";
import { connect } from "react-redux";

import Button from "components/CustomButton/CustomButton.jsx";
import loginPageStyle from "assets/jss/material-kit-react/views/loginPage.jsx";
import image from "assets/img/bg7.jpg";

import { reset_password } from "../../actions/authentication";

class ResetPassword extends React.Component {
  constructor(props) {
    super(props);
    // we use this to make the card to appear after the page has been rendered
    this.state = {
      cardAnimaton: "cardHidden",
      data: {
        password: "",
        confirm_password: ""
      },
      errors: {},
      visible: true
    };

    this.onDismiss = this.onDismiss.bind(this);
  }

  componentDidMount() {
    // we add a hidden class to the card and after 700 ms we delete it and the transition appears
    setTimeout(
      function() {
        this.setState({ cardAnimaton: "" });
      }.bind(this),
      700
    );
  }

  onChange = e =>
    this.setState({
      data: { ...this.state.data, [e.target.name]: e.target.value }
  });

  submit = () => {
    const data = this.state.data;

    const errors = this.validate(data);
    this.setState({ errors });

    const parsed = qs.parse(this.props.history.location.search);
    data["reset_password_token"] = parsed["reset_password_token"]

    if (Object.keys(errors).length === 0) {
      this.props
        .reset_password(data)
        .then(() => this.props.history.push("/"))
        .catch(err => {
          const error = err.response ? err.response.data : err.message;
          this.setState({ errors: { response: error }, visible: true });
        });
    }
  };

  validate = data => {
    const errors = {};
    if (!data.password) errors.password = "Please enter your password";
    if (!data.confirm_password) errors.confirm_password = "Please enter your confirm password";
    if (data.confirm_password !== data.password) errors.equal_password = "password and confirm password is not equal";
    return errors;
  };

  onDismiss() {
    this.setState({ visible: false });
  }

  showErrorResponse = (message) => {
    return (
      <div>
        <Alert
          place={'bc'}
          color="danger"
          isOpen={this.state.visible}
          toggle={this.onDismiss}
        >
          <span>{message}</span>
        </Alert>
      </div>
    )
  }

  render() {
    const { errors } = this.state;
    const { classes } = this.props;

    return (
      <div className="content">
        <div
          className={classes.pageHeader}
          style={{
            backgroundImage: "url(" + image + ")",
            backgroundSize: "cover",
            backgroundPosition: "top center"
          }}
        >
          <div className={classes.container}>
            <Row>
              <Col sm="12" md={{ size: 4, offset: 4 }} >
                <Card className={classes[this.state.cardAnimaton]}>
                  <div className={classes.form}>
                    <CardHeader color="primary" className={classes.cardHeader}>
                      <h4>Reset Password</h4>
                    </CardHeader>
                    <CardBody>

                      <div>
                        <InputGroup style={{ marginBottom: 25}}>
                          <Input onChange={this.onChange} placeholder="Password" type="password" name="password" />
                          <InputGroupAddon addonType="append">
                            <InputGroupText>
                              <i className="nc-icon nc-key-25" />
                            </InputGroupText>
                          </InputGroupAddon>
                        </InputGroup>

                        {
                          errors.password &&
                          <p className="text-danger">{errors.password}</p>
                        }
                      </div>

                      <div>
                        <InputGroup style={{ marginBottom: 25}}>
                          <Input onChange={this.onChange} placeholder="Confirm Password" type="password" name="confirm_password" />
                          <InputGroupAddon addonType="append">
                            <InputGroupText>
                              <i className="nc-icon nc-key-25" />
                            </InputGroupText>
                          </InputGroupAddon>
                        </InputGroup>

                        {
                          (errors.confirm_password || errors.equal_password) &&
                          <p className="text-danger">{errors.equal_password || errors.confirm_password}</p>
                        }
                      </div>

                      <Row>
                        <div className="update ml-auto mr-auto">
                          <Button onClick={this.submit} color="primary" round>Reset Password</Button>
                        </div>
                      </Row>

                      <Row>
                        <div className="update ml-auto mr-auto">
                          <Button color="primary" link round onClick={() => this.props.history.push("/sign_in")}>Sign In</Button>
                        </div>
                      </Row>

                      <CardFooter className={classes.cardFooter}>
                      </CardFooter>
                    </CardBody>
                  </div>
                </Card>

                {
                  errors.response &&
                  this.showErrorResponse(errors.response)
                }
              </Col>
            </Row>

          </div>
        </div>
      </div>
    );
  }
}

ResetPassword.propTypes = {
  history: PropTypes.shape({
    push: PropTypes.func.isRequired
  }).isRequired,
  reset_password: PropTypes.func.isRequired
};

export default withStyles(loginPageStyle)(connect(null, { reset_password })(ResetPassword));
