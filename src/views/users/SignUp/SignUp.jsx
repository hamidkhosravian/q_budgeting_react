import React from "react";
// react plugin used to create a form with multiple steps
import ReactWizard from "react-bootstrap-wizard";

// reactstrap components
import { Col } from "reactstrap";

// wizard steps
import SignUpForm from "./SignUpSteps/SignUpForm.jsx";
import ProfileForm from "./SignUpSteps/ProfileForm.jsx";

var steps = [
  {
    stepName: "Sign Up",
    stepIcon: "nc-icon nc-single-02",
    component: SignUpForm
  },
  {
    stepName: "Profile",
    stepIcon: "nc-icon nc-touch-id",
    component: ProfileForm
  }
];

class SignUp extends React.Component {
  render() {
    return (
      <>
        <div className="content">
          <Col className="mr-auto ml-auto" md="10">
            <ReactWizard
              steps={steps}
              validate
              title="Build User Profile"
              description="This information will let help you to know better your users."
              headerTextCenter
              finishButtonClasses="btn-wd"
              nextButtonClasses="btn-wd"
              previousButtonClasses="btn-wd"
            />
          </Col>
        </div>
      </>
    );
  }
}

export default SignUp;
