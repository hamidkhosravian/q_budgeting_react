import SignIn from "layouts/SignIn/SignIn.jsx";
import ForgotPassword from "layouts/ForgotPassword/ForgotPassword.jsx";
import ResetPassword from "layouts/ResetPassword/ResetPassword.jsx";
import AdminLayout from "layouts/Admin/Admin.jsx";
import UserProfile from "views/users/UserProfile/UserProfile.jsx";

const baseRoutes = [
  {
    path: "/sign_in",
    component: SignIn,
    route: 'guest'
  },
  {
    path: "/reset_password",
    component: ResetPassword,
    route: 'guest'
  },
  {
    path: "/forgot_password",
    component: ForgotPassword,
    route: 'guest'
  },
  {
    path: "/admin",
    component: AdminLayout,
    route: 'route'
  },
  {
    path: "/admin/users/profile/:id",
    component: UserProfile,
    route: 'route'
  },
  {
    path: '/',
    component: "/admin/dashboard",
    route: 'redirect'
  }
]

export default baseRoutes;
